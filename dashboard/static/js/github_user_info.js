let githubUserInfo = document.getElementById('github-user-info')

async function getGithubUserInfo() {
    let url = window.location + "github-user"
    let response = await fetch(url)
    response.json().then(response => {
        console.log(response)
        let repositories_choices = ''
        response.github_repos.forEach(repo => {
            repositories_choices +=
                `<div class="form-check">\
                <input class="form-check-input" name="repo-choice" type="radio" value="${repo.url}">\
                <label class="form-check-label" for="exampleRadios2">\
                ${repo.name}\
                </label>\
                </div>`
        })

        githubUserInfo.innerHTML = `<p class = "text-muted mt-3">logged in as ${response.github_user.login}</p>`
        if (response.repo_url) {
            githubUserInfo.innerHTML += `<p id = "selected-repo-name" class = "text mt-3">selected repo ${response.selected_repo_details.name}</p>`
        }else{
            githubUserInfo.innerHTML += `<p id = "selected-repo-name" class = "text mt-3">please select a repository :</p>`
        }

        githubUserInfo.innerHTML +=
            `<form class="form" id='repo-submiting-form' >\
                <div class="form-group mb-2">`+
            repositories_choices +
            `</div>\
                <button type="submit" class="btn btn-primary mb-2">Select Repo</button>\
            </form>`

        repoUpdateForm()
    })
}

if (githubUserInfo) {
    getGithubUserInfo()
}


function repoUpdateForm() {
    let repoSelectingForm = document.getElementById('repo-submiting-form')
    let selectedRepoName = document.getElementById('selected-repo-name')
    repoSelectingForm.addEventListener('submit', updateRepoInDatabase)
    console.log(repoSelectingForm['repo-choice'])
    async function updateRepoInDatabase(e) {
        e.preventDefault()
        let url = window.location + "github-update-repo/"
        body = {
            'updated_repo': repoSelectingForm['repo-choice'].value
        }
        let response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        })


        response.json().then(response => {
            selectedRepoName.innerHTML = `selected repo ` + response['repo_name']
        })
    }
}
