from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField

# Create your models here.


class RawIssue(models.Model):
    value = JSONField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)


class Issue(models.Model):
    cookies = JSONField()
    abs_uri = models.TextField()
    error_path = models.TextField()
    host = models.TextField()
    port = models.TextField()
    raw_uri = models.TextField()
    browser_info = models.TextField()
    request_ip = models.TextField()
    request_method = models.CharField(max_length=10)
    request_user = models.TextField()
    request_header = JSONField()
    exception_name = models.TextField()
    exception_message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    traceback = JSONField()
    group = models.ForeignKey("Group", models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    rawissue = models.ForeignKey("RawIssue", models.CASCADE)


class Key(models.Model):
    value = models.UUIDField(primary_key=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)


class Group(models.Model):
    name = models.TextField()
    interval_start = models.DateTimeField()
    interval_end = models.DateTimeField()
    event_count = models.BigIntegerField()
    user_count = models.BigIntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    first_issue = models.DateTimeField(auto_now_add=True)
    last_issue = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    github_issue_created = models.BooleanField(default=False)


class GithubIntegration(models.Model):
    token = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    repo_url = models.TextField(null=True)


class SlackIntegration(models.Model):
    token = models.TextField()
    webhook_url = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
