from django.utils import timezone
from sentry.celery import app
from .models import Group
from dashboard import rc

@app.task(name='update-group-event-count')
def update_group_event_count():
    groups = Group.objects.all()
    for group in groups:
        if rc.get(group.name):
            if group.event_count != rc.get(group.name):
                group.event_count = rc.get(group.name)
                group.last_issue = timezone.now()
                group.save()
            # if timezone.now() > group.interval_end:
            #     rc.delete(group.name)
