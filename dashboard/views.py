from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from .models import RawIssue, Key, Issue, Group, SlackIntegration, GithubIntegration
from .services import (
    filter_raw_issue,
    browser_info_parser,
    check_api_key_in_headers,
    valid_uuid,
    store_raw_issue,
    generate_group_sign,
    group_sign_exists,
    create_group,
    inc_group_event_count,
    get_selected_repo_details,
)
import json, uuid, requests


@login_required
def issues(request):
    groups = Group.objects.prefetch_related().filter(user=request.user).order_by('-id').all()
    return render(request, "dashboard/issues.html", {"groups": groups})


@csrf_exempt
def store(request):
    if request.method == "POST":
        api_key = check_api_key_in_headers(request.headers)
        if not api_key:
            return HttpResponse(
                "Umm! Aren't You Missing Something?(API Key)", status=401
            )
        if not valid_uuid(api_key):
            return HttpResponse("Looks Like Your API Key is Invalid.", status=400)
        key = Key.objects.filter(pk=api_key).first()
        if not key:
            return HttpResponse("Unauthorized Request", status=401)
        raw_issue_json = json.loads(request.body)
        filter_raw_issue(store_raw_issue(raw_issue_json, key.user))
        return HttpResponse("Stored", status=201)
    else:
        return HttpResponse("Page Not Found", status=404)


@login_required
def issue_detail(request, id):
    issue = get_object_or_404(Issue.objects.select_related(), pk=id)
    frames = json.loads(issue.traceback)[::-1]
    browser_info = issue.browser_info
    parsed_browser_info = browser_info_parser(browser_info)
    return render(
        request,
        "dashboard/issue_detail.html",
        {"issue": issue, "frames": frames, "browser_info": parsed_browser_info},
    )


@login_required
def prev_group_issue(request, group_id, id):
    group = get_object_or_404(Group.objects.prefetch_related(), pk=group_id)
    issue = get_object_or_404(Issue, pk=id)
    prev_issue = group.issue_set.filter(timestamp__lt=issue.timestamp).last()
    if not prev_issue:
        return redirect("issue_detail", id=id)
    return redirect("issue_detail", id=prev_issue.pk)


@login_required
def next_group_issue(request, group_id, id):
    group = get_object_or_404(Group.objects.prefetch_related(), pk=group_id)
    issue = get_object_or_404(Issue, pk=id)
    next_issue = group.issue_set.filter(timestamp__gt=issue.timestamp).first()
    if not next_issue:
        return redirect("issue_detail", id=id)
    return redirect("issue_detail", id=next_issue.pk)


@login_required
def onboarding(request):
    api_key = Key.objects.get(user=request.user)
    return render(request, "dashboard/onboarding.html", {"api_key": api_key})


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            key = Key.objects.create(value=uuid.uuid1(), user=user)
            return redirect("onboarding")
    else:
        form = UserCreationForm()
    return render(request, "registration/signup.html", {"form": form})


def integrations(request):
    github_integration = GithubIntegration.objects.filter(user=request.user).first()
    slack_integration = SlackIntegration.objects.filter(user=request.user).first()
    context = {
        "github_integrated": True if github_integration else False,
        "slack_integrated": True if slack_integration else False,
    }
    return render(request, "integrations/integrations.html", context=context)


def github_integration(request):
    code = request.GET["code"]
    url = "https://github.com/login/oauth/access_token"
    body = {
        "code": code,
        "client_id": "4198e9c3d7d7c4e026d7",
        "client_secret": "2eb83dfa0e0ad56e2e2aa28ccf8b9ac01a0bc44f",
    }
    headers = {"Accept": "application/json"}
    response_obj = requests.post(url, data=body, headers=headers)
    response = dict(response_obj.json())
    try:
        print(response)
        access_token = response["access_token"]
        if not GithubIntegration.objects.filter(user=request.user):
            print("enter in database")
            GithubIntegration.objects.create(user=request.user, token=access_token)
            print("hi")
            return redirect("integrations")
        else:
            return HttpResponse("you have already given access")
    except:
        print("hi")
        return HttpResponse(response_obj)


def github_logout(request):
    user = GithubIntegration.objects.filter(user=request.user).delete()
    return redirect("integrations")


def github_repos(token):
    url = "https://api.github.com/user/repos"
    print(url)
    try:
        headers = {"Authorization": "token " + token}
        response = requests.get(url, headers=headers)
        return HttpResponse(response)
    except:
        return HttpResponse("something went wrong")


@csrf_exempt
def github_update_repo(request):
    if request.method == "POST":
        repo_url = json.loads(request.body)["updated_repo"]
        user_github = GithubIntegration.objects.filter(user=request.user).first()
        user_github.repo_url = repo_url
        user_github.save()
        token = GithubIntegration.objects.filter(user=request.user).first().token
        repo_name = json.loads(get_selected_repo_details(repo_url, token).content)[
            "name"
        ]
        context = json.dumps({"repo_name": repo_name})
    return HttpResponse(context)


def github_user(request):
    try:
        token = GithubIntegration.objects.filter(user=request.user).first().token
        repo_url = GithubIntegration.objects.filter(user=request.user).first().repo_url
        if repo_url:
            selected_repo_details = get_selected_repo_details(repo_url, token)
            selected_repo_issues = get_github_repo_issues(selected_repo_details, token)
            selected_repo_commits = get_github_repo_commits(
                selected_repo_details, token
            )
            url = "https://api.github.com/user"
            headers = {"Authorization": "token " + token}
            response = requests.get(url, headers=headers)
            repo_response = github_repos(token)
            context = json.dumps(
                {
                    "github_user": json.loads(response.content),
                    "github_repos": json.loads(repo_response.content),
                    "repo_url": repo_url,
                    "selected_repo_details": json.loads(selected_repo_details.content),
                    "selected_repo_issues": json.loads(selected_repo_issues.content),
                    "selected_repo_commits": json.loads(selected_repo_commits.content),
                }
            )
        else:
            url = "https://api.github.com/user"
            headers = {"Authorization": "token " + token}
            response = requests.get(url, headers=headers)
            repo_response = github_repos(token)
            context = json.dumps(
                {
                    "github_user": json.loads(response.content),
                    "github_repos": json.loads(repo_response.content),
                }
            )
        return HttpResponse(context)
    except:
        context = json.dumps({"error": "something went wrong"})
        return HttpResponse(context)


def slack_integration(request):
    url = "https://slack.com/api/oauth.access"
    if "code" in request.GET:
        code = request.GET["code"]
        body = {
            "code": code,
            "client_id": "907032534615.907035536775",
            "client_secret": "0edfb7ebb8168ed1e813480d5ce544e0",
        }
        headers = {"Accept": "application/json"}
        response = requests.post(url, data=body, headers=headers)
        if response.status_code == 200:
            json_res = response.json()
            if not json_res["ok"]:
                return render(
                    request,
                    "integrations/integration_error.html",
                    {"error": json_res["error"]},
                )
            elif json_res["ok"]:
                SlackIntegration.objects.create(
                    token=json_res["access_token"],
                    webhook_url=json_res["incoming_webhook"]["url"],
                    user=request.user,
                )
                return redirect("integrations")
        return render(
            request,
            "integrations/integration_error.html",
            {"error": "Error Processing Request"},
        )
    elif "error" in request.GET:
        error = request.GET["error"]
        return render(request, "integrations/integration_error.html", {"error": error})


def slack_disintegrate(request):
    user = SlackIntegration.objects.filter(user=request.user).delete()
    return redirect("integrations")


def get_github_repo_issues(repo_details, token):
    issues_url = json.loads(repo_details.content)["issues_url"]
    issues_url = issues_url.replace("{/number}", "")
    headers = {"Authorization": "token " + token}
    response = requests.get(issues_url, headers=headers)
    return HttpResponse(response)


def get_github_repo_commits(repo_details, token):
    commits_url = json.loads(repo_details.content)["commits_url"]
    commits_url = commits_url.replace("{/sha}", "")
    print("commit", commits_url)
    headers = {"Authorization": "token " + token}
    response = requests.get(commits_url, headers=headers)
    return HttpResponse(response)
