import asyncio
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from .models import Issue, Group
from asgiref.sync import async_to_sync
import channels.layers
from django.db.models import signals
from django.dispatch import receiver
from django.core import serializers
from . import rc


class IssuesConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)
        print(self.scope["user"].id)
        await self.send(
            {"type": "websocket.accept",}
        )
        group_name = str(self.scope["user"].id)
        await self.channel_layer.group_add(group_name, self.channel_name)

    async def websocket_recieve(self, event):
        print("recieved", event)

    async def websocket_disconnect(self, event):
        print("disconnected", event)
        group_name = str(self.scope["user"].id)
        await self.channel_layer.group_discard(group_name, self.channel_name)

    async def new_issue(self, event):
        print(event)
        await self.send({"type": "websocket.send", "text": json.dumps(event["data"])})

    @staticmethod
    @receiver(signals.post_save, sender=Issue)
    def group_observer(sender, instance, **kwargs):
        layer = channels.layers.get_channel_layer()
        group_name = str(instance.user.id)
        async_to_sync(layer.group_send)(
            group_name, {"type": "new.issue", "data": {'id': instance.id, 'group_id': instance.group.id, 'event_count': rc.get(instance.group.name).decode('utf-8'), 'user_count': instance.group.user_count, 'issue_name': instance.exception_name, 'issue_msg': instance.exception_message, 'error_path': instance.error_path, 'group_interval_start': str(instance.group.interval_start), 'group_interval_end': str(instance.group.interval_end)},},
        )
