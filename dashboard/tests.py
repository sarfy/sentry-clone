from django.test import TestCase, Client
from django.contrib.auth.models import User
from .models import Key, Group
import uuid, time
from . import rc

# Create your tests here.

class GroupTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('sarfy', password='c@rpediem')
        self.key = Key.objects.create(value=uuid.uuid1(), user=self.user)
        rc.flushall()

    def test_details(self):
        for i in range(100):
            response = self.client.post(path='/store/',data={'cookies': {'PGADMIN_LANGUAGE': 'en', 'pga4_session': '075eabc2-73e1-42d4-a8b5-5188bf58970f!Qg0J07ccbC6hvZU21aVN7FENaH0=', 'csrftoken': '9TBtbkEWiwwFNfEGMN0ifzCrOacwWIXU4YUPqSmDDZDZIjbMMd5efkwkxz0P0nhN'}, 'absolute_uri': 'http://localhost:8001/sentry-debug/', 'error_path': '/sentry-debug/', 'host': 'localhost:8001', 'port': '8001', 'raw_uri': 'http://localhost:8001/sentry-debug/', 'browser_info': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4000.3 Safari/537.36', 'request_ip': '127.0.0.1', 'request_method': 'GET', 'request_user': 'AnonymousUser', 'request_header': {'Content-Length': '', 'Content-Type': 'text/plain', 'Host': 'localhost:8001', 'Connection': 'keep-alive', 'Cache-Control': 'max-age=0', 'Upgrade-Insecure-Requests': '1', 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4000.3 Safari/537.36', 'Sec-Fetch-Dest': 'document', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9', 'Sec-Fetch-Site': 'none', 'Sec-Fetch-Mode': 'navigate', 'Sec-Fetch-User': '?1', 'Accept-Encoding': 'gzip, deflate, br', 'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8', 'Cookie': 'PGADMIN_LANGUAGE=en; pga4_session=075eabc2-73e1-42d4-a8b5-5188bf58970f!Qg0J07ccbC6hvZU21aVN7FENaH0=; csrftoken=9TBtbkEWiwwFNfEGMN0ifzCrOacwWIXU4YUPqSmDDZDZIjbMMd5efkwkxz0P0nhN'}, 'exception_name': 'NameError', 'exception_message': "name 'anothernomethod' is not defined", 'timestamp': '2020-01-17 02:18:46.937226', 'middleware_path': '/home/sarfy/projects/typeform-clone', 'traceback': [{'frame': [[111, '            wrapped_callback = self.make_view_atomic(callback)\n'], [112, '            try:\n'], [113, '                response = wrapped_callback(request, *callback_args, **callback_kwargs)\n'], [114, '            except Exception as e:\n'], [115, '                response = self.process_exception_by_middleware(e, request)\n'], [116, '\n'], [117, '        # Complain if the view returned None (a common error).\n'], [118, '        if response is None:\n'], [119, '            if isinstance(callback, types.FunctionType):    # FBV\n'], [120, '                view_name = callback.__name__\n']], 'error_line': 115, 'error_file': '/home/sarfy/projects/typeform-clone/env/lib/python3.7/site-packages/django/core/handlers/base.py', 'local_variables': {'self': '<django.core.handlers.wsgi.WSGIHandler object at 0x7f1cd81ea780>', 'request': "<WSGIRequest: GET '/sentry-debug/'>", 'response': 'None', 'resolver': "<URLResolver 'typeform.urls' (None:None) '^/'>", 'resolver_match': 'ResolverMatch(func=typeform.urls.trigger_error, args=(), kwargs={}, url_name=None, app_names=[], namespaces=[], route=sentry-debug/)', 'callback': '<function trigger_error at 0x7f1cd760e378>', 'callback_args': '()', 'callback_kwargs': '{}', 'middleware_method': '<bound method CsrfViewMiddleware.process_view of <django.middleware.csrf.CsrfViewMiddleware object at 0x7f1cd75d2b00>>', 'wrapped_callback': '<function trigger_error at 0x7f1cd760e378>', 'e': "name 'anothernomethod' is not defined"}, 'function_name': '_get_response'}, {'frame': [[17, 'from django.urls import path, include\n'], [18, 'from django.contrib.auth import views\n'], [19, '\n'], [20, 'def trigger_error(request):\n'], [21, '    anothernomethod()\n'], [22, '    division_by_zero = 1 / 0\n'], [23, '\n'], [24, 'urlpatterns = [\n'], [25, "    path('admin/', admin.site.urls),\n"], [26, "    path('accounts/login/', views.LoginView.as_view(), name='login'),\n"]], 'error_line': 21, 'error_file': '/home/sarfy/projects/typeform-clone/typeform/urls.py', 'local_variables': {'request': "<WSGIRequest: GET '/sentry-debug/'>"}, 'function_name': 'trigger_error'}]}, content_type='application/json', HTTP_X_API_KEY=self.key.pk)
        # print(response.content)
        time.sleep(60)
        group = Group.objects.first()
        print(group.event_count)
        rc.flushall()
        # self.assertEqual(group.event_count, 100)