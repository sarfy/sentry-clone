from django.urls import path
from . import views

urlpatterns = [
    path("", views.issues, name="issues"),
    path("store/", views.store, name="store"),
    path("signup/", views.signup, name="signup"),
    path("issues/<id>/", views.issue_detail, name="issue_detail"),
    path("issues/", views.issues),
    path(
        "groups/<group_id>/issues/<id>/prev", views.prev_group_issue, name="prev_issue"
    ),
    path(
        "groups/<group_id>/issues/<id>/next", views.next_group_issue, name="next_issue"
    ),
    path("onboarding/", views.onboarding, name="onboarding"),
    path("integrations/", views.integrations, name="integrations"),
    path(
        "integrations/github-integration/",
        views.github_integration,
        name="github_integrations",
    ),
    path("integrations/github-logout/", views.github_logout, name="github_logout"),
    path("integrations/github-user/", views.github_user, name="github_user"),
    path("integrations/github-update-repo/", views.github_update_repo),
    path("integrations/github-repos/", views.github_repos, name="github_repos"),
    path(
        "integrations/slack-integration/",
        views.slack_integration,
        name="slack_integration",
    ),
    path(
        "integrations/slack-disintegration/",
        views.slack_disintegrate,
        name="slack_disintegrate",
    ),
]
