from django.utils import timezone
from django.urls import reverse
from .models import RawIssue, Issue, Group, SlackIntegration, GithubIntegration
from hashlib import md5
from datetime import datetime
import json, httpagentparser, requests, uuid
from . import rc
from django.http import HttpResponse


def filter_raw_issue(raw_issue):
    if raw_issue:
        parsed_raw_issue = raw_issue.value
        cookies = parsed_raw_issue.get("cookies")
        abs_uri = parsed_raw_issue.get("absolute_uri")
        error_path = parsed_raw_issue.get("error_path")
        host = parsed_raw_issue.get("host")
        port = parsed_raw_issue.get("port")
        raw_uri = parsed_raw_issue.get("raw_uri")
        browser_info = parsed_raw_issue.get("browser_info")
        request_ip = parsed_raw_issue.get("request_ip")
        request_method = parsed_raw_issue.get("request_method")
        request_user = parsed_raw_issue.get("request_user")
        request_header = parsed_raw_issue.get("request_header")
        exception_name = parsed_raw_issue.get("exception_name")
        exception_message = parsed_raw_issue.get("exception_message")
        timestamp = parsed_raw_issue.get("timestamp")
        traceback = parsed_raw_issue.get("traceback")
        issue = Issue(
            cookies=cookies,
            abs_uri=abs_uri,
            error_path=error_path,
            host=host,
            port=port,
            raw_uri=raw_uri,
            browser_info=browser_info,
            request_ip=request_ip,
            request_method=request_method,
            request_user=request_user,
            request_header=request_header,
            exception_name=exception_name,
            exception_message=exception_message,
            timestamp=timestamp,
            traceback=json.dumps(traceback),
            user=raw_issue.user,
            rawissue=raw_issue,
        )
        group_sign = generate_group_sign(raw_issue.value, raw_issue.user)
        if group_sign_exists(group_sign):
            inc_group_event_count(group_sign)
            group = Group.objects.filter(name=group_sign).first()
            # if github is integrated then create github issue
            if GithubIntegration.objects.filter(user=raw_issue.user).first():
                if (
                    int.from_bytes(rc.get(group_sign), "big") > 4
                    and not group.github_issue_created
                ):
                    create_github_issues(
                        raw_issue.user,
                        f"{exception_name}{error_path} - {exception_message}",
                    )
                    group.github_issue_created = True
                    group.save()
            same_issue_user = Issue.objects.filter(
                request_user=request_user, request_ip=request_ip, group=group
            ).first()
            if not same_issue_user:
                group.user_count += 1
                group.save()
            issue.group = group
        else:
            group = create_group(group_sign, raw_issue.user)
            issue.group = group
        issue.save()
        send_slack_notification(issue)


def browser_info_parser(browser_info_str):
    return httpagentparser.detect(browser_info_str)


def generate_issue_fingerprint(
    issue_exception_name, issue_exception_message, issue_error_path
):
    fingerprint_str = f"{issue_exception_name}-{issue_exception_message}-{issue_error_path}".encode(
        "utf-8"
    )
    fingerprint = md5(fingerprint_str).hexdigest()
    return fingerprint


def send_slack_notification(issue):
    integration = SlackIntegration.objects.filter(user=issue.user).first()
    if integration:
        url = integration.webhook_url
        body = {
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": f"*{issue.exception_name}* {issue.error_path}",
                    },
                },
                {
                    "type": "section",
                    "text": {"type": "mrkdwn", "text": f"{issue.exception_message}"},
                },
                {
                    "type": "actions",
                    "elements": [
                        {
                            "type": "button",
                            "text": {"type": "plain_text", "text": "Check Issue"},
                            "url": f"http://localhost:8000/issues/{issue.pk}",
                        }
                    ],
                },
            ]
        }
        headers = {"Content-type": "application/json"}
        response = requests.post(url, data=json.dumps(body), headers=headers)


def check_api_key_in_headers(headers):
    if "X-API-KEY" in headers:
        return headers.get("X-API-KEY")
    return None


def valid_uuid(api_key):
    try:
        uuid.UUID(api_key)
        return True
    except ValueError as ve:
        return False


def store_raw_issue(raw_issue, user):
    return RawIssue.objects.create(value=raw_issue, user=user)


def generate_group_sign(raw_issue, user):
    error_path = raw_issue.get("error_path")
    exception_name = raw_issue.get("exception_name")
    exception_message = raw_issue.get("exception_message")
    issue_timestamp = datetime.strptime(
        raw_issue.get("timestamp"), "%Y-%m-%d %H:%M:%S.%f"
    )
    issue_fingerprint = generate_issue_fingerprint(
        exception_name, exception_message, error_path
    )
    issue_timestamp_spare_minutes = issue_timestamp.minute % 10
    issue_timestamp_hour = (
        f"0{issue_timestamp.hour}"
        if issue_timestamp.hour < 10
        else issue_timestamp.hour
    )
    issue_timestamp_start_minute = (
        f"0{issue_timestamp.minute - issue_timestamp_spare_minutes}"
        if (issue_timestamp.minute - issue_timestamp_spare_minutes) < 10
        else (issue_timestamp.minute - issue_timestamp_spare_minutes)
    )
    issue_timestamp_end_minute = (
        f"0{(issue_timestamp.minute - issue_timestamp_spare_minutes) + 9}"
        if (issue_timestamp.minute - issue_timestamp_spare_minutes) + 9 < 10
        else (issue_timestamp.minute - issue_timestamp_spare_minutes) + 9
    )
    if (issue_timestamp.minute - issue_timestamp_spare_minutes) + 10 == 60:
        issue_timestamp_hour = (
            "00"
            if int(issue_timestamp_hour) + 1 == 24
            else f"0{int(issue_timestamp_hour) + 1}"
            if int(issue_timestamp_hour) + 1 < 10
            else int(issue_timestamp_hour) + 1
        )
    issue_time_interval = f"{issue_timestamp.date()}::{issue_timestamp_hour}:{issue_timestamp_start_minute}::{issue_timestamp_hour}:{issue_timestamp_end_minute}"
    sign = f"{user.pk}::{issue_fingerprint}::{issue_time_interval}"
    return sign


def group_sign_exists(group_sign):
    key = rc.get(group_sign)
    if key:
        return True
    return False


def create_group(group_sign, user):
    interval_data = group_sign.split("::")
    tz = timezone.get_default_timezone()
    interval_start = tz.localize(
        datetime.strptime(f"{interval_data[2]} {interval_data[3]}", "%Y-%m-%d %H:%M")
    )
    interval_end = tz.localize(
        datetime.strptime(f"{interval_data[2]} {interval_data[4]}", "%Y-%m-%d %H:%M")
    )
    rc.set(group_sign, 1)
    group = Group.objects.create(
        name=group_sign,
        interval_start=interval_start,
        interval_end=interval_end,
        user_count=1,
        event_count=1,
        last_issue=timezone.now(),
        user=user,
    )
    return group


def inc_group_event_count(group_sign):
    rc.incr(group_sign)


def create_github_issues(user, title):
    token = GithubIntegration.objects.filter(user=user).first().token
    repo_url = GithubIntegration.objects.filter(user=user).first().repo_url
    selected_repo_details = get_selected_repo_details(repo_url, token)
    response = create_github_repo_issues(title, selected_repo_details, token)
    return HttpResponse(response)


def create_github_repo_issues(issue_title, repo_details, token):
    issues_url = json.loads(repo_details.content)["issues_url"]
    issues_url = issues_url.replace("{/number}", "")
    headers = {"Authorization": "token " + token}
    body = json.dumps({"title": issue_title})
    response = requests.post(issues_url, headers=headers, data=body)
    return HttpResponse(response)


def get_selected_repo_details(repo_url, token):
    try:
        headers = {"Authorization": "token " + token}
        response = requests.get(repo_url, headers=headers)
        return HttpResponse(response)
    except:
        return HttpResponse("something went wrong")
